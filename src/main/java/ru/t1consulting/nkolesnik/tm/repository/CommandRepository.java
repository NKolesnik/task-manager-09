package ru.t1consulting.nkolesnik.tm.repository;

import ru.t1consulting.nkolesnik.tm.constant.ArgumentConst;
import ru.t1consulting.nkolesnik.tm.constant.TerminalConst;
import ru.t1consulting.nkolesnik.tm.model.Command;

public class CommandRepository implements ru.t1consulting.nkolesnik.tm.api.ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info.");

    private static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show terminal commands.");

    private static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show application version.");

    private static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show system info.");

    private static final Command EXIT = new Command(
            TerminalConst.EXIT,
            "Close application.\n");

    private static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Show argument list."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Show commands list."
    );

    private final Command[] commands = new Command[]{
            INFO,
            ABOUT,
            VERSION,
            HELP,
            COMMANDS,
            ARGUMENTS,
            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return commands;
    }

}
