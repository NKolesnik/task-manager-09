package ru.t1consulting.nkolesnik.tm.api;

public interface ICommandController {

    void showWelcome();

    void showHelp();

    void showCommands();

    void showArguments();

    void showAbout();

    void showVersion();

    void showSystemInfo();

    void showErrorCommand(String arg);

    void showErrorArgument(String arg);

    void close();

}

