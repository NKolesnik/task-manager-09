package ru.t1consulting.nkolesnik.tm.api;

import ru.t1consulting.nkolesnik.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
