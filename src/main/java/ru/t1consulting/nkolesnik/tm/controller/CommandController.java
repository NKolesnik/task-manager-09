package ru.t1consulting.nkolesnik.tm.controller;

import ru.t1consulting.nkolesnik.tm.api.ICommandController;
import ru.t1consulting.nkolesnik.tm.api.ICommandService;
import ru.t1consulting.nkolesnik.tm.model.Command;
import ru.t1consulting.nkolesnik.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showSystemInfo() {
        System.out.println("[INFO]");
        final Runtime runtime = Runtime.getRuntime();
        final int processors = runtime.availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = runtime.freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryOutput = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryOutput);
        final long totalMemory = runtime.totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Nikolay Kolesnik");
        System.out.println("E-mail: kolesnik.nik.vrn@gmail.com");
    }

    @Override
    public void showVersion() {
        System.out.println("1.6.0");
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) {
            String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) {
            String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) {
            System.out.println(command);
        }
    }

    @Override
    public void showErrorArgument(String arg) {
        System.err.printf("Error! This argument `%s` not supported... \n", arg);
    }

    @Override
    public void showErrorCommand(String arg) {
        System.err.printf("Error! This command `%s` not supported... \n", arg);
    }

    @Override
    public void close() {
        System.exit(0);
    }

    @Override
    public void showWelcome() {
        System.out.println("** Welcome to Task-Manager **");
    }

}
